var app = angular.module('universidadApp', ['ngRoute', 'ui.mask', 'jcs-autoValidate']);

app.run([
    'defaultErrorMessageResolver',
    function (defaultErrorMessageResolver) {
        // To change the root resource file path
        defaultErrorMessageResolver.setI18nFileRootPath('js/lib/lang');
        defaultErrorMessageResolver.setCulture('es-CO');
        defaultErrorMessageResolver.getErrorMessages().then(function (errorMessages) {
            errorMessages['edadMinima'] = 'Debe de ser mayor a {0} años de edad';
            errorMessages['edadMaxima'] = 'Debe de ser menor a {0} años de edad';
            errorMessages['rut'] = 'El rut debe ser correcto';
            errorMessages['telefono'] = 'Debe ingresar el teléfono';
        });
    }
]);

app.controller('mainCtrl', ['$scope', '$http', function ($scope, $http) {

        $scope.menuSuperior = 'parciales/menu.html';
        $scope.rutMask = "99.999.999-*";
        $scope.telefonoMask = "+999-9999-9999";

        $scope.setActive = function (Opcion) {

            $scope.mInicio = "";
            $scope.mProfesores = "";
            $scope.mAlumnos = "";

            $scope[Opcion] = "active";

        };

    }]);

app.filter('telefono', function () {
    return function (numero) {
        return numero.substring(0, 4) + "-" + numero.substring(4);
    };
});

function ValidarRutValidatorDirective(defaultErrorMessageResolver) {
    defaultErrorMessageResolver.getErrorMessages().then(function (errorMessages) {
        errorMessages['validarRut'] = 'El rut debe ser correcto';
    });

    return {
        restrict: 'A',
        require: 'ngModel',
        scope: {
            validarRut: '=validarRut'
        },
        link: function (scope, element, attributes, ngModel) {
            ngModel.$validators.validarRut = function (modelValue) {
                rut = modelValue;

                if (rut !== undefined) {
                    // Despejar Puntos
                    var valor = rut.replace('.', '');
                    // Despejar Guión
                    valor = valor.replace('-', '');

                    // Aislar Cuerpo y Dígito Verificador
                    cuerpo = valor.slice(0, -1);
                    dv = valor.slice(-1).toUpperCase();

                    // Formatear RUN
                    rut = cuerpo + '-' + dv;

                    // Si no cumple con el mínimo ej. (n.nnn.nnn)
                    if (cuerpo.length < 7) {
                        return false;
                    }

                    // Calcular Dígito Verificador
                    suma = 0;
                    multiplo = 2;

                    // Para cada dígito del Cuerpo
                    for (i = 1; i <= cuerpo.length; i++) {

                        // Obtener su Producto con el Múltiplo Correspondiente
                        index = multiplo * valor.charAt(cuerpo.length - i);

                        // Sumar al Contador General
                        suma = suma + index;

                        // Consolidar Múltiplo dentro del rango [2,7]
                        if (multiplo < 7) {
                            multiplo = multiplo + 1;
                        } else {
                            multiplo = 2;
                        }

                    }

                    // Calcular Dígito Verificador en base al Módulo 11
                    dvEsperado = 11 - (suma % 11);

                    // Casos Especiales (0 y K)
                    dv = (dv == 'K') ? 10 : dv;
                    dv = (dv == 0) ? 11 : dv;

                    // Validar que el Cuerpo coincide con su Dígito Verificador
                    if (dvEsperado != dv) {
                        return false;
                    }

                    // Si todo sale bien, eliminar errores (decretar que es válido)
                    return true;
                } else {
                    return "";
                }
            };

            scope.$watch('validarRut', function () {
                ngModel.$validate();
            });
        }
    };
}

ValidarRutValidatorDirective.$inject = [
    'defaultErrorMessageResolver'
];

app.directive('validarRut', ValidarRutValidatorDirective);

//app.filter('rut', function () {
//    return function (rut) {
//        return `${rut.substring(0,2)}.${rut.substring(2,3)}.${rut.substring(5,3)}-${rut.substring(8)}`;
//    };
//});